<!DOCTYPE html>
<html lang="en">
  <?php include('include/headerscript.php'); ?>
   <body>
      <div class="main-page-wrapper">
           <?php include('include/header.php'); ?>
       
         <div class="theme-inner-banner section-spacing">
            <div class="overlay">
               <div class="container">
                  <h2>Services</h2>
               </div>
               <!-- /.container -->
            </div>
            <!-- /.overlay -->
         </div>
         <!-- /.theme-inner-banner -->


         <div class="container">
  <div class="row">
  <ul>
    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 Services-tab  item">
      <div class="folded-corner service_tab_1">
        <div class="text">
          <i class="fa fa-image fa-5x fa-icon-image"></i>
            <p class="item-title">
                <h3> Designing</h3>
              </p><!-- /.item-title -->
          <p>
            This is an amazing set of animated accordions based completely on CSS. They come oriented both vertically and horizontally in order to fit properly in your project. In order to see the slides, 
          </p>
        </div>
      </div>
      </div>
    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 Services-tab item">
      <div class="folded-corner service_tab_1">
        <div class="text">
          <i class="fa fa-lightbulb-o fa-5x fa-icon-image" ></i>
            <p class="item-title">
              <h3> Developing</h3>
            </p><!-- /.item-title -->
            <p>
              This is an amazing set of animated accordions based completely on CSS. They come oriented both vertically and horizontally in order to fit properly in your project. In order to see the slides, 
          </p>
        </div>
      </div>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 Services-tab item">
      <div class="folded-corner service_tab_1">
        <div class="text">
          <i class="fa fa-truck fa-5x fa-icon-image"></i>
            <p class="item-title">
              <h3> Marketing</h3>
            </p><!-- /.item-title -->
          <p>
            This is an amazing set of animated accordions based completely on CSS. They come oriented both vertically and horizontally in order to fit properly in your project. In order to see the slides, 
          </p>
        </div>
      </div>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 Services-tab item">
      <div class="folded-corner service_tab_1">
        <div class="text">
          <i class="fa fa-diamond fa-5x fa-icon-image"></i>
            <p class="item-title">
              <h3> Branding</h3>
            </p><!-- /.item-title -->
          <p>
            This is an amazing set of animated accordions based completely on CSS. They come oriented both vertically and horizontally in order to fit properly in your project. In order to see the slides, 
          </p>
        </div>
      </div>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 Services-tab item">
      <div class="folded-corner service_tab_1">
      <div class="text">
        <i class="fa fa-line-chart fa-5x fa-icon-image"></i>
          <p class="item-title">
            <h3>Analytics</h3>
          </p><!-- /.item-title -->
          <p>
            This is an amazing set of animated accordions based completely on CSS. They come oriented both vertically and horizontally in order to fit properly in your project. In order to see the slides, 
          </p>
        </div>
      </div>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 Services-tab item">
      <div class="folded-corner service_tab_1">
        <div class="text">
          <i class="fa fa-mobile fa-5x fa-icon-image"></i>
            <p class="item-title">
              <h3>Mobil Apps</h3>
            </p><!-- /.item-title -->
          <p>
            This is an amazing set of animated accordions based completely on CSS. They come oriented both vertically and horizontally in order to fit properly in your project. In order to see the slides, 
          </p>
        </div>
      </div>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 Services-tab item">
        <div class="folded-corner service_tab_1">
          <div class="text">
            <i class="fa fa-money fa-5x fa-icon-image"></i>
              <p class="item-title">
                <h3> e-commerce</h3>
              </p><!-- /.item-title -->
            <p>
              This is an amazing set of animated accordions based completely on CSS. They come oriented both vertically and horizontally in order to fit properly in your project. In order to see the slides, 
            </p>
        </div>
      </div>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 Services-tab item">
      <div class="folded-corner service_tab_1">
        <div class="text">
          <i class="fa fa-bullhorn fa-5x fa-icon-image"></i>
            <p class="item-title">
              <h3> Support</h3>
            </p><!-- /.item-title -->
          <p>
            This is an amazing set of animated accordions based completely on CSS. They come oriented both vertically and horizontally in order to fit properly in your project. In order to see the slides, 
          </p>
        </div>
      </div>
    </div>
     </ul>
  </div>
</div>
         
       


 <?php include('include/footer.php'); ?>

         <?php include('include/footerscript.php'); ?>
   </body>

</html>