 <header class="header-two">
            <div class="top-header">
               <div class="container">
                  <div class="row">
                     <div class="col-md-6 col-sm-8 col-12">
                        <ul class="left-widget">
                           <li><span class="custom-call"></span> (+91) 9429812012</li>
                        </ul>
                     </div>
                     <div class="col-md-6 col-sm-4 col-12">
                        <ul class="left-widget">
                           <li style="float:right;">Email: info@deltawebsolution.com</li>
                        </ul>
                     </div>
                  </div>
                  <!-- /.row -->
               </div>
               <!-- /.container -->
            </div>
            <!-- /.top-header -->
            <div class="theme-menu-wrapper">
               <div class="container">
                  <div class="bg-wrapper clearfix">
                     <div class="logo float-left"><a href="index.html"><img src="images/logo/logo.png" style="height: 82px;"  alt=""></a></div>
                     <!-- ============== Menu Warpper ================ -->
                     <div class="menu-wrapper float-right">
                        <nav id="mega-menu-holder" class="clearfix">
                           <ul class="clearfix">
                              <li class="active"><a href="index.php">Home</a>
                              </li>
                              <li>
                                 <a href="#">About Us</a>
                                 <ul class="dropdown">
                                    <li><a href="about.php">About Our Company</a></li>
                                    <li><a href="aboutwork.php">Work Flow</a></li>
                                 </ul>
                              </li>
                              <li>
                                 <a href="services.php">Soluctions</a>
                                 <ul class="dropdown">
                                    <li><a href="#">Website Development</a></li>
                                    <li><a href="#">Software Development</a></li>
                                    <li>
                                       <a href="#">Cloud Computing</a>
                                       <ul>
                                          <li><a href="#">Domain Services</a></li>
                                          <li><a href="#">Web Hosting</a></li>
                                       </ul>
                                    </li>
                                    <li>
                                       <a href="#">Graphics Design</a>
                                       <ul>
                                          <li ><a href="">Logo Design</a></li>
                                          <li><a href="#">Brochures</a></li>
                                          <li><a href="#">Flyer</a></li>
                                          <li><a href="#">Box Packing design</a></li>
                                          <li><a href="#">Banners</a></li>
                                          <li><a href="#">Prininting Services</a></li>
                                       </ul>
                                    </li>
                                    <li><a href="#">CRM Solutions</a></li>
                                    <li><a href="#">CMS Solutions</a></li>
                                    <li>
                                       <a href="#">E-commerce Solutions</a>
                                       <ul>
                                          <li><a href="#">Standard Ecomerce</a></li>
                                          <li><a href="#">Basic Ecomerce</a></li>
                                       </ul>
                                    </li>
                                    <li>
                                       <a href="#">ERP Solutions</a>
                                       <ul>
                                          <li><a href="#">Custo ERP</a></li>
                                          <li><a href="#">Radymade ERP</a></li>
                                       </ul>
                                    </li>
                                    <li><a href="#">Search Engine Optimization</a></li>
                                    <li>
                                       <a href="#">Mobile App Development</a>
                                       <ul>
                                          <li><a href="#">Android App</a></li>
                                          <li><a href="#">IOS App</a></li>
                                       </ul>
                                    </li>
                                 </ul>
                              </li>
                              <li>
                                 <a href="#">Technology</a>
                                 <ul class="dropdown">
                                    <li>
                                       <a href="#">Mobile</a>
                                       <ul>
                                          <li><a href="#">IOS</a></li>
                                          <li><a href="#">Android</a></li>
                                       </ul>
                                    </li>
                                    <li>
                                       <a href="#">Web</a>
                                       <ul>
                                          <li><a href="#">Html</a></li>
                                          <li><a href="#">PHP</a></li>
                                          <li><a href="#">ASP .Net</a></li>
                                          <li><a href="#">Python</a></li>
                                          <li><a href="#">Ajax</a></li>
                                       </ul>
                                    </li>
                                    <li>
                                       <a href="#">Framework</a>
                                       <ul>
                                          <li><a href="#">PHP Core</a></li>
                                          <li><a href="#">Cake Php</a></li>
                                          <li><a href="#">Laravel</a></li>
                                          <li><a href="#">Wordpress</a></li>
                                       </ul>
                                    </li>
                                 </ul>
                              </li>
                              <li><a href="#$">Portfolio</a>
                              <li><a href="#">Inquiry</a>   
                              <li><a href="#contact">Contact</a></li>
                           </ul>
                        </nav>
                        <!-- /#mega-menu-holder -->
                     </div>
                     <!-- /.menu-wrapper -->
                  </div>
                  <!-- /.bg-wrapper -->
               </div>
               <!-- /.container -->
            </div>
            <!-- /.theme-menu-wrapper -->
         </header>