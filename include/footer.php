 <footer class="theme-footer-one">
            <div class="top-footer">
               <div class="container">
                  <div class="row">
                     <div class="col-xl-4 col-md-4 col-sm-6 footer-list">
                        <h6 class="title">Services</h6>
                        <ul>
                           <li><a href="#">Web Designing</a></li>
                           <li><a href="#">Web Development</a></li>
                           <li><a href="#">ERP System</a></li>
                           <li><a href="#">Mobile App Development</a></li>
                           <li><a href="#">Informative Website</a></li>
                           <li><a href="#">E-comerce</a></li>
                        </ul>
                     </div>
                     <!-- /.footer-recent-post -->
                     <div class="col-xl-4 col-md-4 col-sm-6 footer-list">
                        <h6 class="title">SOLUTIONS</h6>
                        <ul>
                           <li><a href="#">Brochure & Logo Design</a></li>
                           <li><a href="#">SEO</a></li>
                           <li><a href="#">Grafic Design</a></li>
                           <li><a href="#">Web Hosting & Domain </a></li>
                           <li><a href="#">Dynamic Website</a></li>
                           <li><a href="#">Static Website</a></li>
                        </ul>
                     </div>
                     <!-- /.footer-list -->
                     <div class="col-xl-4 col-md-4 col-sm-6 contact-widget">
                        <h6 class="title">CONTACT</h6>
                        <ul>
                           <li>
                              <i class="flaticon-multimedia-1"></i>
                              <a href="#">info@deltawebsolution.com</a>
                           </li>
                           <li>
                              <i class="fa fa-phone" aria-hidden="true"></i>
                              <a href="#">(+91) 9429812012, 9723840340</a>
                           </li>
                        </ul>
                     </div>
                  </div>
                  <!-- /.row -->
               </div>
               <!-- /.container -->
            </div>
            <!-- /.top-footer -->
            <div class="bottom-footer">
               <div class="container">
                  <div class="row">
                     <div class="col-md-6 col-12">
                        <p>&copy; Design and Developed by Deltawebsolution.</p>
                     </div>
                     <div class="col-md-6 col-12">
                        <ul>
                           <li><a href="#">About</a></li>
                           <li><a href="#">Solutions</a></li>
                           <li><a href="#">FAQ’s</a></li>
                           <li><a href="#">Contact</a></li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
            <!-- /.bottom-footer -->
         </footer>