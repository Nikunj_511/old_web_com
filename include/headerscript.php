 <head>
      <meta charset="UTF-8">
      <!-- For IE -->
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <!-- For Resposive Device -->
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <!-- For Window Tab Color -->
      <meta name="keywords" content="deltaweb,deltawebsolution,deltawebsolutions,HTML,CSS,XML,JavaScript,php,software,website">
      <meta name="description" content="search more about websitedesigning deltawebsolution - we are creative web designers and developers, Android apps">
      <meta name="author" content="Deltaweb Solution">
      <!-- Windows Phone -->
      <meta name="msapplication-navbutton-color" content="">
      <!-- iOS Safari -->
      <meta name="apple-mobile-web-app-status-bar-style" content="">
      <title>Deltawbsolution</title>
      <!-- Favicon -->
      <link rel="icon" type="image/png" sizes="56x56" href="images/logo/logo.png">
      <!-- Main style sheet -->
      <link rel="stylesheet" type="text/css" href="css/style.css">
      <link rel="stylesheet" type="text/css" href="fonts/icon/font/custom_icon.css">
      <link rel="stylesheet" type="text/css" href="css/common_class.css">
      <link rel="stylesheet" type="text/css" href="css/custom_style.css">
      <!-- responsive style sheet -->
      <link rel="stylesheet" type="text/css" href="css/responsive.css">
      <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.1/css/all.css" integrity="sha384-O8whS3fhG2OnA5Kas0Y9l3cfpmYjapjI0E4theH4iuMD+pLhbf6JI0jIMfYcK3yZ" crossorigin="anonymous">
      <link href="https://fonts.googleapis.com/css?family=Sunflower:300" rel="stylesheet">
   </head>