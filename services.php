<!DOCTYPE html>
<html lang="en">
   <?php include('include/headerscript.php'); ?>
   <body>
      <div class="main-page-wrapper">
      <?php include('include/header.php'); ?>
      <div class="theme-inner-banner section-spacing">
         <div class="overlay">
            <div class="container">
               <h2>Services</h2>
            </div>
            <!-- /.container -->
         </div>
         <!-- /.overlay -->
      </div>
      <!-- /.theme-inner-banner -->
      <section id="setservices">
         <div class="container">
            <div class="row">
               <div class="col-xl-3 col-md-4 col-sm-6 col-lg-4 Services-tab  item">
                  <div class="folded-corner service_tab_1">
                     <div class="text">
                        <i class="custom-web-design fa-3x fa-icon-image"></i>
                        <p class="item-title">
                        <h3> Website Development</h3>
                        </p><!-- /.item-title -->
                        <p>
                           This is an amazing set of animated accordions based completely on CSS.  
                        </p>
                        <!-- <a class="read-more" href="">Read More</a> -->
                     </div>
                  </div>
               </div>
               <div class="col-xl-3 col-md-4 col-sm-6 col-lg-4 Services-tab item">
                  <div class="folded-corner service_tab_1">
                     <div class="text">
                        <i class="custom-software fa-3x fa-icon-image" ></i>
                        <p class="item-title">
                        <h3> Software Development</h3>
                        </p><!-- /.item-title -->
                        <p>
                           This is an amazing set of animated accordions based completely on CSS.  
                        </p>
                     </div>
                  </div>
               </div>
               <div class="col-xl-3 col-md-4 col-sm-6 col-lg-4 Services-tab item">
                  <div class="folded-corner service_tab_1">
                     <div class="text">
                        <i class="custom-hosting fa-3x fa-icon-image"></i>
                        <p class="item-title">
                        <h3> Cloud Computing</h3>
                        </p><!-- /.item-title -->
                        <p>
                           This is an amazing set of animated accordions based completely on CSS.  
                        </p>
                     </div>
                  </div>
               </div>
               <div class="col-xl-3 col-md-4 col-sm-6 col-lg-4 Services-tab item">
                  <div class="folded-corner service_tab_1">
                     <div class="text">
                        <i class="custom-computer-graphic fa-3x fa-icon-image"></i>
                        <p class="item-title">
                        <h3> Graphics Design</h3>
                        </p><!-- /.item-title -->
                        <p>
                           This is an amazing set of animated accordions based completely on CSS.  
                        </p>
                     </div>
                  </div>
               </div>
               <div class="col-xl-3 col-md-4 col-sm-6 col-lg-4 Services-tab item">
                  <div class="folded-corner service_tab_1">
                     <div class="text">
                        <i class="custom-payment-method fa-3x fa-icon-image"></i>
                        <p class="item-title">
                        <h3>CRM Solutions</h3>
                        </p><!-- /.item-title -->
                        <p>
                           This is an amazing set of animated accordions based completely on CSS.  
                        </p>
                     </div>
                  </div>
               </div>
               <div class="col-xl-3 col-md-4 col-sm-6 col-lg-4 Services-tab item">
                  <div class="folded-corner service_tab_1">
                     <div class="text">
                        <i class="custom-web-design-1 fa-3x fa-icon-image"></i>
                        <p class="item-title">
                        <h3>CMS Solutions</h3>
                        </p><!-- /.item-title -->
                        <p>
                           This is an amazing set of animated accordions based completely on CSS.  
                        </p>
                     </div>
                  </div>
               </div>
               <div class="col-xl-3 col-md-4 col-sm-6 col-lg-4 Services-tab item">
                  <div class="folded-corner service_tab_1">
                     <div class="text">
                        <i class="custom-online-store fa-3x fa-icon-image"></i>
                        <p class="item-title">
                        <h3> e-commerce</h3>
                        </p><!-- /.item-title -->
                        <p>
                           This is an amazing set of animated accordions based completely on CSS.  
                        </p>
                     </div>
                  </div>
               </div>
               <div class="col-xl-3 col-md-4 col-sm-6 col-lg-4 Services-tab item">
                  <div class="folded-corner service_tab_1">
                     <div class="text">
                        <i class="custom-analysis	 fa-3x fa-icon-image"></i>
                        <p class="item-title">
                        <h3> ERP Solutions</h3>
                        </p><!-- /.item-title -->
                        <p>
                           This is an amazing set of animated accordions based completely on CSS.  
                        </p>
                     </div>
                  </div>
               </div>
               <div class="col-xl-3 col-md-4 col-sm-6 col-lg-4 Services-tab item">
                  <div class="folded-corner service_tab_1">
                     <div class="text">
                        <i class="fas fa-chart-line fa-3x fa-icon-image"></i>
                        <p class="item-title">
                        <h3>SEO</h3>
                        </p><!-- /.item-title -->
                        <p>
                           This is an amazing set of animated accordions based completely on CSS.  
                        </p>
                     </div>
                  </div>
               </div>
               <div class="col-xl-3 col-md-4 col-sm-6 col-lg-4 Services-tab item">
                  <div class="folded-corner service_tab_1">
                     <div class="text">
                        <i class="custom-buying-on-smartphone fa-3x fa-icon-image"></i>
                        <p class="item-title">
                        <h3>Mobile App</h3>
                        </p><!-- /.item-title -->
                        <p>
                           This is an amazing set of animated accordions based completely on CSS.  
                        </p>
                     </div>
                  </div>
               </div>
               <div class="col-xl-3 col-md-4 col-sm-6 col-lg-4 Services-tab item">
                  <div class="folded-corner service_tab_1">
                     <div class="text">
                        <i class="custom-responsive-design-symbol fa-3x fa-icon-image"></i>
                        <p class="item-title">
                        <h3> Redesign</h3>
                        </p><!-- /.item-title -->
                        <p>
                           This is an amazing set of animated accordions based completely on CSS.  
                        </p>
                     </div>
                  </div>
               </div>
               <div class="col-xl-3 col-md-4 col-sm-6 col-lg-4 Services-tab item">
                  <div class="folded-corner service_tab_1">
                     <div class="text">
                        <i class="custom-digital-marketing	 fa-3x fa-icon-image"></i>
                        <p class="item-title">
                        <h3> SMO</h3>
                        </p><!-- /.item-title -->
                        <p>
                           This is an amazing set of animated accordions based completely on CSS.  
                        </p>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <?php include('include/footer.php'); ?>
      <?php include('include/footerscript.php'); ?>
   </body>
</html>