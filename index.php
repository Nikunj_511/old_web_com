<!DOCTYPE html>
<html lang="en">
   <?php include('include/headerscript.php'); ?>
   <body>
      <div class="main-page-wrapper">
         <!-- ===================================================
            Loading Transition
            ==================================================== -->
         <!-- <div id="loader-wrapper">
            <div id="loader"></div>
            </div> -->
         <!-- 
            =============================================
               Theme Header One
            ============================================== 
            -->
         <?php include('include/header.php'); ?>
         <!-- /.header-two -->
         <!-- 
            =============================================
               Theme Main Banner
            ============================================== 
            -->
         <div id="home">
            <div id="theme-main-banner" class="banner-one">
               <!--<div data-src="images/slider/28.png">-->
               <!--</div>-->
               <!--<div data-src="images/slider/17.png">-->
               <!--</div>-->
               <!--<div data-src="images/slider/19.jpg">-->
               <!--</div>-->
               <div data-src="images/logo/13.png">
               </div>
               <div data-src="images/logo/11.png">
               </div>
               <div data-src="images/logo/10.jpg">
               </div>
               <div data-src="images/logo/19.png">
               </div>
               <!--<div data-src="images/logo/3.jpg">-->
               <!--</div>-->
               <div data-src="images/logo/15.jpg">
               </div>
               <div data-src="images/logo/20.jpg">
               </div>
               <div data-src="images/logo/21.png">
               </div>
               <div data-src="images/logo/22.png">
               </div>
               <div data-src="images/logo/23.jpg">
               </div>
            </div>
            <!-- /#theme-main-banner -->
         </div>
         <div class="callout-banner">
            <div class="container clearfix">
               <h3 class="title">Provide solutions for WebSite</h3>
               <p>We have expertise in Website Designing, eCommerce Solutions, SEO & Internet Marketing, Mobile App Development, Logo Designing, Graphic Designing, Business Presentations, Content Writing, Branding, Web Hosting and more!</p>
               <a href="#" class="theme-button-one">FREE QUOTES</a>
            </div>
         </div>
         <!-- 
            =============================================
               Top Feature
            ============================================== 
            -->
         <div class="top-feature section-spacing">
            <div class="top-features-slide">
               <div class="item">
                  <div class="main-content" style="background:#fafafa;">
                     <img src="images/icon/1.png" alt="">
                     <h4><a href="#">Customer</a></h4>
                     <p>The east side to a deluxe apartment in move on up to the east side</p>
                  </div>
                  <!-- /.main-content -->
               </div>
               <!-- /.item -->
               <div class="item">
                  <div class="main-content" style="background:#f6f6f6;">
                     <img src="images/icon/2.png" alt="">
                     <h4><a href="#">Quality</a></h4>
                     <p>The east side to a deluxe apartment in move on up to the east side</p>
                  </div>
                  <!-- /.main-content -->
               </div>
               <!-- /.item -->
               <div class="item">
                  <div class="main-content" style="background:#efefef;">
                     <img src="images/icon/3.png" alt="">
                     <h4><a href="#">Efficiency</a></h4>
                     <p>The east side to a deluxe apartment in move on up to the east side</p>
                  </div>
                  <!-- /.main-content -->
               </div>
               <!-- /.item -->
               <div class="item">
                  <div class="main-content" style="background:#e9e9e9;">
                     <img src="images/icon/4.png" alt="">
                     <h4><a href="#">Reliability</a></h4>
                     <p>The east side to a deluxe apartment in move on up to the east side</p>
                  </div>
                  <!-- /.main-content -->
               </div>
               <!-- /.item -->
            </div>
            <!-- /.top-features-slide -->
         </div>
         <!-- /.top-feature -->
         <!-- 
            =============================================
               About Company
            ============================================== 
            -->
         <div id="about">
            <div class="about-compnay section-spacing">
               <div class="container">
                  <div class="row">
                     <h2 align="center">About Our Company</h2>
                  </div>
                  <div class="row">
                     <div class="col-lg-6 col-6">
                        <img src="images/logo/35.jpg" style="padding-top:40px;" alt="">
                        <!--<img src="images/logo/34.png" style="padding-top:40px;" alt="">-->
                     </div>
                     <div class="col-lg-6 col-12">
                        <div class="text">
                           <div class="theme-title-one">
                              <p align="jusify">We design great user experiences on the web. We have a certified team of professionals who are expert in strategizing, developing, executing and delivering results par excellence. Our web design team visualizes your requirement with sales perspective to build a website that not just looks great but can convert your site visitors into customers.</p>
                            
                           </div>
                           <!-- /.theme-title-one -->
                           <ul class="mission-goal clearfix">
                              <li>
                                 <i class="icon flaticon-star"></i>
                                 <h4>Vision</h4>
                              </li>
                              <li>
                                 <i class="icon flaticon-medal"></i>
                                 <h4>Missions</h4>
                              </li>
                              <li>
                                 <i class="icon flaticon-target"></i>
                                 <h4>Goals</h4>
                              </li>
                           </ul>
                           <!-- /.mission-goal -->
                        </div>
                        <!-- /.text -->
                     </div>
                     <!-- /.col- -->
                  </div>
                  <!-- /.row -->
               </div>
               <!-- /.container -->
            </div>
            <!-- /.about-compnay -->
         </div>


           <div class="feature-banner section-spacing">
            <div class="opacity">
               <div class="container">
                  <h2>Are you ready for a great web presence? Call us now at +91 9429812012 or</h2>
                  <a href="#" class="theme-button-one">GET A QUOTES</a>
               </div>
               <!-- /.container -->
            </div>
            <!-- /.opacity -->
         </div>


         <section id="setservices">
            <div class="container">
               <div class="theme-title-one" style="margin-bottom:40px;">
                  <h2>Our SERVICES</h2>
               </div>
               <div class="row">
                  <div class="col-xl-3 col-md-4 col-sm-6 col-lg-4 Services-tab  item">
                     <div class="folded-corner service_tab_1">
                        <div class="text">
                           <i class="custom-web-design fa-4x fa-icon-image"></i>
                           <p class="item-title">
                           <h3> Website Development</h3>
                           </p><!-- /.item-title -->
                           <p>
                              This is an amazing set of animated accordions based completely on CSS.  
                           </p>
                           <!-- <a class="read-more" href="">Read More</a> -->
                        </div>
                     </div>
                  </div>
                  <div class="col-xl-3 col-md-4 col-sm-6 col-lg-4 Services-tab item">
                     <div class="folded-corner service_tab_1">
                        <div class="text">
                           <i class="custom-software fa-4x fa-icon-image" ></i>
                           <p class="item-title">
                           <h3> Software Development</h3>
                           </p><!-- /.item-title -->
                           <p>
                              This is an amazing set of animated accordions based completely on CSS.  
                           </p>
                        </div>
                     </div>
                  </div>
                  <div class="col-xl-3 col-md-4 col-sm-6 col-lg-4 Services-tab item">
                     <div class="folded-corner service_tab_1">
                        <div class="text">
                           <i class="custom-hosting fa-4x fa-icon-image"></i>
                           <p class="item-title">
                           <h3> Cloud Computing</h3>
                           </p><!-- /.item-title -->
                           <p>
                              This is an amazing set of animated accordions based completely on CSS.  
                           </p>
                        </div>
                     </div>
                  </div>
                  <div class="col-xl-3 col-md-4 col-sm-6 col-lg-4 Services-tab item">
                     <div class="folded-corner service_tab_1">
                        <div class="text">
                           <i class="custom-computer-graphic fa-4x fa-icon-image"></i>
                           <p class="item-title">
                           <h3> Graphics Design</h3>
                           </p><!-- /.item-title -->
                           <p>
                              This is an amazing set of animated accordions based completely on CSS.  
                           </p>
                        </div>
                     </div>
                  </div>
                  <div class="col-xl-3 col-md-4 col-sm-6 col-lg-4 Services-tab item">
                     <div class="folded-corner service_tab_1">
                        <div class="text">
                           <i class="custom-payment-method fa-4x fa-icon-image"></i>
                           <p class="item-title">
                           <h3>CRM Solutions</h3>
                           </p><!-- /.item-title -->
                           <p>
                              This is an amazing set of animated accordions based completely on CSS.  
                           </p>
                        </div>
                     </div>
                  </div>
                  <div class="col-xl-3 col-md-4 col-sm-6 col-lg-4 Services-tab item">
                     <div class="folded-corner service_tab_1">
                        <div class="text">
                           <i class="custom-web-design-1 fa-4x fa-icon-image"></i>
                           <p class="item-title">
                           <h3>CMS Solutions</h3>
                           </p><!-- /.item-title -->
                           <p>
                              This is an amazing set of animated accordions based completely on CSS.  
                           </p>
                        </div>
                     </div>
                  </div>
                  <div class="col-xl-3 col-md-4 col-sm-6 col-lg-4 Services-tab item">
                     <div class="folded-corner service_tab_1">
                        <div class="text">
                           <i class="custom-online-store fa-4x fa-icon-image"></i>
                           <p class="item-title">
                           <h3> e-commerce</h3>
                           </p><!-- /.item-title -->
                           <p>
                              This is an amazing set of animated accordions based completely on CSS.  
                           </p>
                        </div>
                     </div>
                  </div>
                  <div class="col-xl-3 col-md-4 col-sm-6 col-lg-4 Services-tab item">
                     <div class="folded-corner service_tab_1">
                        <div class="text">
                           <i class="custom-analysis   fa-4x fa-icon-image"></i>
                           <p class="item-title">
                           <h3> ERP Solutions</h3>
                           </p><!-- /.item-title -->
                           <p>
                              This is an amazing set of animated accordions based completely on CSS.  
                           </p>
                        </div>
                     </div>
                  </div>
                  <div class="col-xl-3 col-md-4 col-sm-6 col-lg-4 Services-tab item">
                     <div class="folded-corner service_tab_1">
                        <div class="text">
                           <i class="fas fa-chart-line fa-4x fa-icon-image"></i>
                           <p class="item-title">
                           <h3>SEO</h3>
                           </p><!-- /.item-title -->
                           <p>
                              This is an amazing set of animated accordions based completely on CSS.  
                           </p>
                        </div>
                     </div>
                  </div>
                  <div class="col-xl-3 col-md-4 col-sm-6 col-lg-4 Services-tab item">
                     <div class="folded-corner service_tab_1">
                        <div class="text">
                           <i class="custom-buying-on-smartphone fa-4x fa-icon-image"></i>
                           <p class="item-title">
                           <h3>Mobile App</h3>
                           </p><!-- /.item-title -->
                           <p>
                              This is an amazing set of animated accordions based completely on CSS.  
                           </p>
                        </div>
                     </div>
                  </div>
                  <div class="col-xl-3 col-md-4 col-sm-6 col-lg-4 Services-tab item">
                     <div class="folded-corner service_tab_1">
                        <div class="text">
                           <i class="custom-responsive-design-symbol fa-4x fa-icon-image"></i>
                           <p class="item-title">
                           <h3> Redesign</h3>
                           </p><!-- /.item-title -->
                           <p>
                              This is an amazing set of animated accordions based completely on CSS.  
                           </p>
                        </div>
                     </div>
                  </div>
                  <div class="col-xl-3 col-md-4 col-sm-6 col-lg-4 Services-tab item">
                     <div class="folded-corner service_tab_1">
                        <div class="text">
                           <i class="custom-digital-marketing   fa-4x fa-icon-image"></i>
                           <p class="item-title">
                           <h3> SMO</h3>
                           </p><!-- /.item-title -->
                           <p>
                              This is an amazing set of animated accordions based completely on CSS.  
                           </p>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </section>
         <!--
            =====================================================
               Testimonial Slider
            =====================================================
            -->
          
 <section id="work-process" class="work-process section_new">
      <div class="work-bg-pattern d-none d-lg-inline-block">
         <img src="images/process/work_process.png" alt="" class="img-fluid">
      </div>
      <!-- End Work BG Pattern -->
      <div class="container">
         
         <!-- End Row -->
         <div class="row">
            <div class="col-md-3">
               <div class="tw-work-process">
                  <div class="process-wrapper d-table">
                     <div class="process-inner d-table-cell">
                        <img src="images/icon/process1.png" alt="" class="img-fluid">
                     </div>
                  </div>
                  <!-- End process wrapper -->
                  <p>01. Research Project</p>
               </div>
               <!-- End Tw work process -->
            </div>
            <!-- End Col -->
            <div class="col-md-3">
               <div class="tw-work-process">
                  <div class="process-wrapper bg-orange d-table">
                     <div class="process-inner d-table-cell">
                        <img src="images/icon/process2.png" alt="" class="img-fluid">
                     </div>
                  </div>
                  <!-- End Process Wrapper -->
                  <p>02. Find Ideas</p>
               </div>
               <!-- End Word Process -->
            </div>
            <!-- End Col -->
            <div class="col-md-3">
               <div class="tw-work-process">
                  <div class="process-wrapper bg-blue d-table">
                     <div class="process-inner d-table-cell">
                        <img src="images/icon/process3.png" alt="" class="img-fluid">
                     </div>
                  </div>
                  <!-- End Process Wrapper -->
                  <p>03. Start Optimize</p>
               </div>
               <!-- End Work Process -->
            </div>
            <!-- End Col -->
            <div class="col-md-3">
               <div class="tw-work-process">
                  <div class="process-wrapper bg-yellow d-table">
                     <div class="process-inner d-table-cell">
                        <img src="images/icon/process4.png" alt="" class="img-fluid">
                     </div>
                  </div>
                  <!-- End PRocess Wrapper -->
                  <p>04. Reach Target</p>
               </div>
               <!-- End Work Process -->
            </div>
           

         </div>
         <!-- End Row -->
      </div>
      <!-- End Container -->
   </section>
       


   <section id="tw-facts" class="tw-facts">
      <div class="container">
         <div class="row">
            <div class="col-md-3 text-center">
               <div class="tw-facts-box">
                  <div class="facts-img">
                     <img src="images/icon/fact1.png" alt="" class="img-fluid">
                  </div>
                  <!-- End Fatcs image -->
                  <div class="facts-content">
                     <h4 class="facts-title">Active clients</h4>
                     <span class="counter">200</span>
                     <sup>+</sup>
                  </div>
                  <!-- Facts Content End -->
               </div>
               <!-- Facts Box End -->
            </div>
            <!-- Col End -->
            <div class="col-md-3 text-center">
               <div class="tw-facts-box">
                  <div class="facts-img">
                     <img src="images/icon/fact2.png" alt="" class="img-fluid">
                  </div>
                  <!-- End Facts Image -->
                  <div class="facts-content">
                     <h4 class="facts-title">Projects Done</h4>
                     <span class="counter">570</span>
                     <sup>+</sup>
                  </div>
                  <!-- End Facts Content -->
               </div>
               <!-- End Facts Box -->
            </div>
            <!-- Col End -->
            <div class="col-md-3 text-center">
               <div class="tw-facts-box">
                  <div class="facts-img">
                     <img src="images/icon/fact3.png" alt="" class="img-fluid">
                  </div>
                  <!-- End Facts Image -->
                  <div class="facts-content">
                     <h4 class="facts-title">Success Rate</h4>
                     <span class="counter">98</span>
                     <sup>%</sup>
                  </div>
                  <!-- End Facts Content -->
               </div>
               <!-- End Facts Box -->
            </div>
            <!-- Col End -->
            <div class="col-md-3 text-center">
               <div class="tw-facts-box">
                  <div class="facts-img">
                     <img src="images/icon/fact4.png" alt="" class="img-fluid">
                  </div>
                  <!-- End Facts Image -->
                  <div class="facts-content">
                     <h4 class="facts-title">Awards</h4>
                     <span class="counter">50</span>
                     <sup>+</sup>
                  </div>
                  <!-- End Facts Content -->
               </div>
               <!-- End Facts Box -->
            </div>
            <!-- Col End -->
         </div>
         <!-- Row End -->
      </div>
      <!-- Container End -->
   </section>
   <!-- Facts End -->

     <section id="tw-testimonial" class="tw-testimonial section_new p-b-50">
      <div class="container">
         <div class="row align-items-center justify-content-center">
            <div class="col-md-6 wow fadeInLeft" data-wow-duration="1s">
               <div class="tw-testimonial-content text-center p-t-30">
                  <h2 class="section-title">Love From Clients</h2>
                  <span class="animate-border border-green tw-mt-20 tw-mb-40 ml-auto mr-auto"></span>
               </div>
               <div class="tw-testimonial-carousel owl-carousel">
                  <div class="tw-testimonial-wrapper">
                     <div class="testimonial-bg testimonial-bg-orange">
                        <div class="testimonial-icon">
                           <img src="images/icon-image/testimonial1.png" alt="" class="img-fluid">
                        </div>
                     </div>
                     <!-- End Testimonial bg -->
                     <div class="testimonial-text">
                        <p>Start working with an company that can do provide every thing at you need to generate awareness,
                           drive traffic, connect with</p>
                        <div class="testimonial-meta">
                           <h4>
                              Jason Stattham
                              <small>CEO Microhost</small>
                           </h4>
                           <i class="icon icon-quote2"></i>
                        </div>
                        <!-- End Testimonial Meta -->
                     </div>
                     <!-- End testimonial text -->
                  </div>
                  <!-- End Tw testimonial wrapper -->
                  <div class="tw-testimonial-wrapper">
                     <div class="testimonial-bg testimonial-bg-orange">
                        <div class="testimonial-icon">
                           <img src="images/icon-image/testimonial1.png" alt="" class="img-fluid">
                        </div>
                     </div>
                     <!-- End Testimonial bg -->
                     <div class="testimonial-text">
                        <p>Start working with an company that can do provide every thing at you need to generate awareness,
                           drive traffic, connect with</p>
                        <div class="testimonial-meta">
                           <h4>
                              Jason Stattham
                              <small>CEO Microhost</small>
                           </h4>
                           <i class="icon icon-quote2"></i>
                        </div>
                        <!-- End Testimonial meta -->
                     </div>
                     <!-- End Testimonial Text -->
                  </div>
                  <!-- End Tw testimonial wrapper -->
                  <div class="tw-testimonial-wrapper">
                     <div class="testimonial-bg testimonial-bg-orange">
                        <div class="testimonial-icon">
                           <img src="images/icon-image/testimonial1.png" alt="" class="img-fluid">
                        </div>
                     </div>
                     <!-- end testimonial bg -->
                     <div class="testimonial-text">
                        <p>Start working with an company that can do provide every thing at you need to generate awareness,
                           drive traffic, connect with</p>
                        <div class="testimonial-meta">
                           <h4>
                              Jason Stattham
                              <small>CEO Microhost</small>
                           </h4>
                           <i class="icon icon-quote2"></i>
                        </div>
                        <!-- End testimonial meta -->
                     </div>
                     <!-- End testimonial text -->
                  </div>
                  <!-- End tw testimonial wrapper -->
               </div>
               <!-- End Tw testimonial carousel -->
            </div>
            <!-- End Col -->
         </div>
         <!-- End Row -->
      </div>
      <!-- End Container -->
   </section>
   <!-- End TW testimonial -->

   <div id="contact">   
         <div class="consultation-form section-spacing">
            <div class="container">
               <div class="theme-title-one p-t-40">
                  <h2>Request A Quote</h2>
               </div> <!-- /.theme-title-one -->
               <div class="clearfix main-content no-gutters row">
                  <div class="col-xl-6 col-lg-5 col-12"><div class="img-box"></div></div>
                  <div class="col-xl-6 col-lg-7 col-12">
                     <div class="form-wrapper">
                        <form action="#" class="theme-form-one">
                           <div class="row">
                              <div class="col-md-6"><input type="text" placeholder="Name *"></div>
                              <div class="col-md-6"><input type="text" placeholder="Phone *"></div>
                              <div class="col-md-6"><input type="email" placeholder="Email *"></div>
                              <div class="col-md-6">
                                 <select class="form-control" id="exampleSelect1">
                                    <option>Service you’re looking for?</option>
                                    <option>Static Website</option>
                                    <option>Dynamic Website</option>
                                    <option>E-comerce Website</option>
                                    <option>ERP System</option>
                                    <option>Grapic Design</option>
                                    <option>Printing Design</option>
                                    <option>Mobile App</option>
                                    <option>SEO</option>
                                  </select>
                              </div>
                              <div class="col-12"><textarea placeholder="Message"></textarea></div>
                           </div> <!-- /.row -->
                           <button class="theme-button-one">GET A QUOTES</button>
                        </form>
                     </div> <!-- /.form-wrapper -->
                  </div> <!-- /.col- -->
               </div> <!-- /.main-content -->
            </div> <!-- /.container -->
         </div> <!-- /.consultation-form -->
         </div>



  

 

          
         <!-- /.partner-section -->
         <section style="background-image: url('images/footer.png');height:200px !important;"></section>
         <!--
            =====================================================
               Footer
            =====================================================
            -->
         <?php include('include/footer.php'); ?>
         <?php include('include/footerscript.php'); ?>
      </div>
   </body>
</html>